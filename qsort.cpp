#include <iostream>
#include <vector>
#include <stdio.h>
using namespace std;

void swap(int* a, int* b) 
{ 
    int t = *a; 
    *a = *b; 
    *b = t; 
}

int partition (auto& A, int l, int r) 
{ 
    int pivot = A[r];    // pivot 
    int i = (l - 1);  // Index of smaller element 
  
    for (int j = l; j <= r- 1; j++) 
    { 
        // If current element is smaller than or 
        // equal to pivot 
        if (A[j] <= pivot) 
        { 
            i++;    // increment index of smaller element 
            swap(&A[i], &A[j]); 
        } 
    } 
    swap(&A[i + 1], &A[r]); 
    return (i + 1); 
}

// Input: A - contains input data to be sorted
//        l - leftmost index
//        r - rightmost index
int quickSort(auto& A, int l, int r )
{
  int count = 0;
  
  if (l < r) 
    { 
        /* pi is partitioning index, arr[p] is now 
           at right place */
        int pi = partition(A, l, r); 
  
        // Separately sort elements before 
        // partition and after partition 
        quickSort(A, l, pi - 1); 
        quickSort(A, pi + 1, r); 
    }
  
  return count;
}

void print(auto& A)
{
	for(int i = 0; i < A.size(); i++){
		cout << A[i] << ' ';
	}
}


int main()
{
  vector<int> inputs;
  int input;

   cerr<<"Welcome to \"quickSort Analysis\". We first need some input data."<<endl;
   cerr<<"To end input type Ctrl+D (followed by Enter)"<<endl<<endl;

   
 
    while(cin>>input)//read an unknown number of inputs from keyboard
    {
       inputs.push_back(input);
    }

   cerr<<endl<<"|  Number of inputs | Number of comparisons |"<<endl;
   cerr<<"|\t"<<inputs.size();
   cout<<"| "<<quickSort(inputs, 0, inputs.size() -1);
   
   
    
   cerr<<"\t|"<<endl<<endl<<"Program finished."<<endl<<endl;
   cout << "sorted vector: ";
   print(inputs);
   cout << endl << endl;

    return 0;
}
